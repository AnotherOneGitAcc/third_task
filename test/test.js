const { expect } = require("chai");
const { ethers, network } = require("hardhat");

describe("Vesting System Tests", function () {
    let Token;
    let Vesting;

    let hardhatToken;
    let hardhatVesting;

    let owner;
    let address1;
    let address2;

    // Similar to deploy rules
    this.beforeEach(async function () {
        [owner, address1, address2] = await hre.ethers.getSigners();

        Token = await hre.ethers.getContractFactory("Token");
        Vesting = await hre.ethers.getContractFactory("Vesting");

        hardhatToken = await Token.deploy();
        hardhatVesting = await Vesting.deploy(hardhatToken.address);

        await hardhatToken.deployed();
        await hardhatVesting.deployed();
    });

    describe("Vesting before start", function () {
        it("Should grant addr1 with vesting struct and see his balance", async function () {
            await hardhatToken.mint(owner.address, 100);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.grant(address1.address, 100, 10);

            const addr1Balance = await hardhatVesting.connect(address1).seeBalance();
            expect(addr1Balance).to.be.equal(100);
        });

        it("Should grant addr1 with vesting struct and see his vesting period in days", async function () {
            await hardhatToken.mint(owner.address, 100);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.grant(address1.address, 100, 10);

            const addr1VestingPeriod = await hardhatVesting.connect(address1).seeVestingPeriod();
            expect(addr1VestingPeriod).to.be.equal(10);
        });

        it("Should grant addr1 with vesting struct and reset his vesting period in days", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 200);

            await hardhatVesting.grant(address1.address, 100, 10);
            await hardhatVesting.grant(address1.address, 100, 15);

            const addr1VestingPeriod = await hardhatVesting.connect(address1).seeVestingPeriod();
            expect(addr1VestingPeriod).to.be.equal(15);
        });

        it("Should stake 100 tokens (1 day)", async function () {
            await hardhatToken.mint(owner.address, 100);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.grant(address1.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            const addr1Reward = await hardhatVesting.earned(address1.address);
            expect(addr1Reward).to.be.equal(100);
        });

        it("Should grant addr1 and addr2 with vesting structs and they should stake both 50 tokens", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 200);

            await hardhatVesting.grant(address1.address, 100, 10);
            await hardhatVesting.grant(address2.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            const addr1Reward = await hardhatVesting.earned(address1.address);
            const addr2Reward = await hardhatVesting.earned(address1.address);
            expect(addr1Reward).to.be.equal(addr2Reward).to.be.equal(50);
        });

        it("Should grant addr1 and addr2 with vesting structs and then add amount to addr1", async function () {
            await hardhatToken.mint(owner.address, 400);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 400);

            await hardhatVesting.grant(address1.address, 100, 10);
            await hardhatVesting.grant(address2.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            await hardhatVesting.grant(address1.address, 200, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            const addr1Reward = await hardhatVesting.earned(address1.address);
            expect(addr1Reward).to.be.equal(125);
        });

        it("Should grant addr1 and addr2 and owner with vesting structs and they should stake both 100 tokens in 3 days", async function () {
            await hardhatToken.mint(owner.address, 300);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 300);

            await hardhatVesting.grant(owner.address, 100, 10);
            await hardhatVesting.grant(address1.address, 100, 10);
            await hardhatVesting.grant(address2.address, 100, 10);
            await network.provider.send("evm_increaseTime", [3 * 86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            const ownerReward = await hardhatVesting.earned(owner.address);
            const addr1Reward = await hardhatVesting.earned(address1.address);
            const addr2Reward = await hardhatVesting.earned(address1.address);
            expect(addr1Reward).to.be.equal(addr2Reward).to.be.equal(ownerReward).to.be.equal(100);
        });
    });

    describe("Failed transactions vesting before start", function () {
        it("Should revert from getting reward", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);
            await hardhatToken.transfer(hardhatVesting.address, 100);

            await hardhatVesting.grant(owner.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            await expect(
                hardhatVesting.getReward()
            ).to.be.revertedWith("You can get your reward for staking only after start");
        });

        it("Should revert from withdraw", async function () {
            await hardhatToken.mint(owner.address, 100);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.grant(owner.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            await expect(
                hardhatVesting.withdraw(100)
            ).to.be.revertedWith("You can get your tokens only after start");
        });
    });


    describe("Vesting after start", function () {
        it("Should stake 100 tokens (1 day) and get it", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 200);
            await hardhatVesting.addTokensToRewardPool(100);

            await hardhatVesting.grant(address1.address, 100, 10);
            await network.provider.send("evm_increaseTime", [86400]);

            await hardhatVesting.start();

            await hardhatVesting.connect(address1).getReward();

            const addr1Reward = await hardhatToken.balanceOf(address1.address);
            expect(addr1Reward).to.be.equal(100);
        });

        it("Should participate in vesting and get reward and withdraw tokens", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 200);
            await hardhatVesting.addTokensToRewardPool(100);

            await hardhatVesting.grant(address1.address, 100, 1);
            await hardhatVesting.start();

            await network.provider.send("evm_increaseTime", [86400]);

            await hardhatVesting.connect(address1).getReward();
            await hardhatVesting.connect(address1).withdraw(100);

            const addr1Reward = await hardhatToken.balanceOf(address1.address);
            expect(addr1Reward).to.be.equal(200);
        });
    });

    describe("Failed transactions vesting after start", function () {
        it("Should revert from withdraw cause tokens is blocked accoarding to shedule", async function () {
            await hardhatToken.mint(owner.address, 200);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.grant(address1.address, 100, 10);
            await hardhatVesting.start();

            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            await expect(
                hardhatVesting.connect(address1).withdraw(100)
            ).to.be.revertedWith("You can withdraw only tokens that available accoarding to vesting shedule");
        });
    });

    describe("Staking after start", function () {
        it("Should create vesting structure tokens", async function () {
            await hardhatToken.mint(owner.address, 100);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);

            await hardhatVesting.start();

            await hardhatVesting.stake(100);

            const addr1Balance = await hardhatVesting.seeBalance();
            expect(addr1Balance).to.be.equal(100);
        });

        it("Should stake tokens and get reward and withdraw", async function () {
            await hardhatToken.mint(address1.address, 100);
            await hardhatToken.mint(owner.address, 100);

            await hardhatToken.connect(address1).increaseAllowance(hardhatVesting.address, 100);

            await hardhatToken.increaseAllowance(hardhatVesting.address, 100);
            await hardhatVesting.addTokensToRewardPool(100);

            await hardhatVesting.start();

            await hardhatVesting.connect(address1).stake(100);
            await network.provider.send("evm_increaseTime", [86400]);

            // To deploy new block time
            await hardhatToken.mint(owner.address, 1);

            await hardhatVesting.connect(address1).getReward();
            await hardhatVesting.connect(address1).withdraw(100);

            const addr1Balance = await hardhatToken.balanceOf(address1.address);
            expect(addr1Balance).to.be.equal(200);
        });
    });

    describe("APY and TVL", function () {
        it("Watch APY", async function () {
            await hardhatToken.mint(owner.address, 46500);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 46500);
            await hardhatVesting.addTokensToRewardPool(36500);

            await hardhatVesting.start();

            await hardhatVesting.stake(10000);

            const APY = await hardhatVesting.APY();
            expect(APY).to.be.equal(365);
        });

        it("Watch APY defining this sum from another account", async function () {
            await hardhatToken.mint(owner.address, 46500);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 46500);
            await hardhatVesting.addTokensToRewardPool(36500);

            await hardhatVesting.start();

            await hardhatVesting.stake(10000);

            const APY = await hardhatVesting.APYWithAmount(10000);
            expect(APY).to.be.equal(182);
        });

        it("TVL", async function () {
            await hardhatToken.mint(owner.address, 10000);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 10000);

            await hardhatVesting.start();

            await hardhatVesting.stake(10000);

            const valueLocked = await hardhatVesting.TVL();
            expect(valueLocked).to.be.equal(10000);
        });

        it("Watch APY if rewardPool is zero", async function () {
            await hardhatToken.mint(owner.address, 10000);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 10000);

            await hardhatVesting.start();

            await hardhatVesting.stake(10000);

            const APY = await hardhatVesting.APY();
            expect(APY).to.be.equal(0);
        });

        it("Watch APY with amount if rewardPool is less than needed", async function () {
            await hardhatToken.mint(owner.address, 46500);
            await hardhatToken.increaseAllowance(hardhatVesting.address, 46500);
            await hardhatVesting.addTokensToRewardPool(18250);

            await hardhatVesting.start();

            await hardhatVesting.stake(10000);

            const APY = await hardhatVesting.APYWithAmount(10000);
            expect(APY).to.be.equal(91);
        });
    });
});