// SPDX-License-Identifier: MIT
pragma solidity ^0.8;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Vesting is Ownable {
    IERC20 public token;

    uint256 public rewardRate = 100;
    uint256 public periodReward = 60 * 60 * 24; // 1 day
    uint256 secondsInDay = 60 * 60 * 24;

    uint256 public lastUpdateTime;
    uint256 public rewardPerTokenStored;

    struct StakingStruct {
        uint256 userRewardPerTokenPaid;
        uint256 reward;
        uint256 _balance;
        uint256 vestingPeriodInDays;
    }

    mapping(address => StakingStruct) stakings;

    uint256 private _totalSupply;
    uint256 private _rewardPool;

    bool _start = false;
    uint256 startVestTime;

    constructor(address _token) {
        token = IERC20(_token);
    }

    function setRewardRate(uint256 reward) public onlyOwner {
        rewardRate = reward;
    }

    function addTokensToRewardPool(uint256 amount) public {
        if (token.transferFrom(msg.sender, address(this), amount)) {
            _rewardPool += amount;
        }
    }

    function rewardPerToken() public view returns (uint256) {
        if (_totalSupply == 0) {
            return 0;
        }
        return
            rewardPerTokenStored +
            (((block.timestamp - lastUpdateTime) * rewardRate * 1e18) /
                periodReward /
                _totalSupply);
    }

    function earned(address account) public view returns (uint256) {
        return
            ((stakings[account]._balance *
                (rewardPerToken() - stakings[account].userRewardPerTokenPaid)) /
                1e18) + stakings[account].reward;
    }

    function seeBalance() external view returns (uint256) {
        return stakings[msg.sender]._balance;
    }

    function seeVestingPeriod() external view returns (uint256) {
        return stakings[msg.sender].vestingPeriodInDays;
    }

    modifier updateReward(address account) {
        rewardPerTokenStored = rewardPerToken();
        lastUpdateTime = block.timestamp;

        stakings[account].reward = earned(account);
        stakings[account].userRewardPerTokenPaid = rewardPerTokenStored;
        _;
    }

    function initAlllocations(
        address[] memory _accounts,
        uint256[] memory _amounts,
        uint256[] memory _periodsInDays
    ) public onlyOwner {
        require(
            _accounts.length == _amounts.length &&
                _accounts.length == _periodsInDays.length,
            "Need arrays to be the same length"
        );
        require(
            _accounts.length <= 15,
            "Size of arrays should be less than 15"
        );
        require(!isStarted(), "Admin can grant before start");

        for (uint256 i = 0; i < _accounts.length; i++) {
            this.grant(_accounts[i], _amounts[i], _periodsInDays[i]);
        }
    }

    function grant(
        address _account,
        uint256 _amount,
        uint256 _periodInDays
    ) external updateReward(_account) {
        require(!isStarted(), "Admin can grant before start");
        require(
            msg.sender == owner() || msg.sender == address(this),
            "This can be calles only by admin or by this contract"
        );
        require(_account != address(0), "Need to add an existing address");
        if (token.transferFrom(msg.sender, address(this), _amount)) {
            _totalSupply += _amount;
            stakings[_account]._balance += _amount;
            stakings[_account].vestingPeriodInDays = _periodInDays;
        }
    }

    function start() public onlyOwner {
        _start = true;
        startVestTime = block.timestamp;
    }

    function isStarted() public view returns (bool) {
        return _start;
    }

    // Works as stake function and add to stake function
    function stake(uint256 _amount) external updateReward(msg.sender) {
        require(isStarted(), "Usual users can start stake after start");
        if (token.transferFrom(msg.sender, address(this), _amount)) {
            _totalSupply += _amount;
            stakings[msg.sender]._balance += _amount;
        }
    }

    function withdraw(uint256 _amount) external updateReward(msg.sender) {
        require(isStarted(), "You can get your tokens only after start");
        require(
            _amount <= stakings[msg.sender]._balance,
            "You can withdraw only that you have blocked on this account"
        );

        if (stakings[msg.sender].vestingPeriodInDays == 0) {
            if (token.transfer(msg.sender, _amount)) {
                _totalSupply -= _amount;
                stakings[msg.sender]._balance -= _amount;
            }
        } else {
            require(
                _amount <= availableToWithdrawn(msg.sender),
                "You can withdraw only tokens that available accoarding to vesting shedule"
            );
            if (token.transfer(msg.sender, _amount)) {
                _totalSupply -= _amount;
                stakings[msg.sender]._balance -= _amount;
            }
        }
    }

    function availableToWithdrawn(address account)
        public
        view
        returns (uint256)
    {
        if (!isStarted()) {
            return 0;
        }

        uint256 daysSpent = (block.timestamp - startVestTime) / secondsInDay;

        if (daysSpent >= stakings[account].vestingPeriodInDays) {
            return stakings[account]._balance;
        } else {
            return
                (stakings[account]._balance * daysSpent) /
                stakings[account].vestingPeriodInDays;
        }
    }

    function getReward() external updateReward(msg.sender) {
        require(
            isStarted(),
            "You can get your reward for staking only after start"
        );
        uint256 reward = stakings[msg.sender].reward;
        require(
            reward <= _rewardPool,
            "Admin need to add tokens on this account"
        );
        if (token.transfer(msg.sender, reward)) {
            stakings[msg.sender].reward = 0;
            _rewardPool -= reward;
        }
    }

    function APY() public view returns (uint256) {
        if (_totalSupply == 0) {
            return 0;
        }
        if (rewardRate * 365 > _rewardPool) {
            return (_rewardPool * 100) / _totalSupply;
        } else {
            return (rewardRate * 365 * 100) / _totalSupply;
        }
    }

    function APYWithAmount(uint256 amount) public view returns (uint256) {
        if (rewardRate * 365 > _rewardPool) {
            return (_rewardPool * 100) / (_totalSupply + amount);
        } else {
            return (rewardRate * 365 * 100) / (_totalSupply + amount);
        }
    }

    function TVL() public view returns (uint256) {
        return _totalSupply;
    }
}
