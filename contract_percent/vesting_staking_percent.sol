// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./token.sol";

// Fix reward per year, mint/burn tokens

contract VestingStakingPercent is Ownable {
    struct VestingStruct {
        uint256 timeForStaking;
        uint256 periodInDays;
        uint256 amount;
        uint256 withdrawn;
    }

    struct StakingStruct {
        uint256 timeForStaking;
        uint256 amount;
        uint256 withdrawn;
    }

    mapping(address => VestingStruct) vestings;
    mapping(address => StakingStruct) stakings;

    mapping(uint256 => address) usersVesting;
    uint256 users_counter_vesting;

    mapping(uint256 => address) usersStaking;
    uint256 users_counter_staking;

    uint256 seconds_in_day = 60 * 60 * 24;
    // Should be less than 100% or you can change it to days/months/weeks remaking some functions
    uint8 public percent_per_year;
    // Vesting starts for all users in the same time
    uint256 public start_vest_time;

    bool start = false;

    Token private token;

    // On contract initialization, owner was set in Owner construcor, define token here
    constructor(address token_) {
        token = Token(token_);
    }

    // Before vesting started p.3, can set own reward for staking
    function setPercentForStaking(uint8 percent_per_year_) public onlyOwner {
        require(!isStarted(), "You can change it before vesting started");
        percent_per_year = percent_per_year_;
    }

    function startVesting() public onlyOwner {
        start = true;
        start_vest_time = block.timestamp;
    }

    function isStarted() public view returns (bool) {
        return start;
    }

    // VESTING FUNCTIONS

    function _grant(
        address account,
        uint256 start_time,
        uint256 period,
        uint256 amount
    ) internal {
        vestings[account] = VestingStruct(start_time, period, amount, 0);
        usersVesting[users_counter_vesting] = account;
        users_counter_vesting++;
    }

    // Before vesting started p.1, Admin functionality p.1, defining the vesting accounts
    // Before vesting started p.2, staking should be initialized instantly
    // Before vesting started p.4, Admin functionality p.3, you can set different periods, but only in linear strategy to unlock
    function grant(
        address account,
        uint256 amount,
        uint256 period
    ) public onlyOwner {
        require(!isGranted(account), "Already granted");
        require(!isStarted(), "Admin can call this before start");
        _grant(account, block.timestamp, period, amount);
    }

    // Admin functionality p.4 p.2, before and after start you can call this
    function addAditionalAmountVesting(
        address account,
        uint256 additionalReward
    ) public onlyOwner {
        require(
            isGranted(account),
            "Admin can add additional reward only on granted accounts"
        );
        vestings[account].amount +=
            additionalReward +
            stakingTokensFromVesting(account);
        vestings[account].timeForStaking = block.timestamp;
    }

    function stakingTokensFromVesting(address account)
        public
        view
        returns (uint256)
    {
        require(isGranted(account), "Need to be granted");
        require(isStarted(), "You can interact only after start of vesting");

        uint256 staked_tokens = ((vestings[account].amount -
            vestings[account].withdrawn) *
            (block.timestamp - vestings[account].timeForStaking) *
            percent_per_year) /
            seconds_in_day /
            365 /
            100;

        return staked_tokens;
    }

    // After vesting started p.1 p.2, you can see available tokens
    function availableFromVesting(address account)
        public
        view
        returns (uint256)
    {
        require(isGranted(account), "Need to be granted");
        require(isStarted(), "You can interact only after start of vesting");

        uint256 availableTokensVesting;

        uint256 daysSpent = (block.timestamp - start_vest_time) /
            seconds_in_day;

        if (daysSpent >= vestings[account].periodInDays) {
            availableTokensVesting = vestings[account].amount;
        } else {
            availableTokensVesting =
                (vestings[account].amount * daysSpent) /
                vestings[account].periodInDays;
        }

        uint256 availableTokens = availableTokensVesting +
            stakingTokensFromVesting(account) -
            vestings[account].withdrawn;

        return availableTokens;
    }

    // After vesting started p.1 p.2, you can get available tokens
    function getTokensFromVesting(address account, uint256 tokens) public {
        require(isGranted(account), "Need to be granted");
        require(isStarted(), "You can interact only after start of vesting");
        require(
            account == msg.sender,
            "Only owner of vesting account can get tokens"
        );

        uint256 stakedTokens = stakingTokensFromVesting(account);

        require(tokens >= stakedTokens, "Need to take at least staked tokens");
        require(
            tokens <= availableFromVesting(account),
            "Too much, see how much is available in availableFromVesting function"
        );

        token.mint(account, tokens);

        vestings[account].amount =
            vestings[account].amount +
            stakedTokens -
            tokens;
        vestings[account].withdrawn =
            vestings[account].withdrawn +
            tokens -
            stakedTokens;

        vestings[account].timeForStaking = block.timestamp;
    }

    // STAKING FUNCTIONS

    function _stake(
        address account,
        uint256 start_time,
        uint256 amount
    ) internal {
        stakings[account] = StakingStruct(start_time, amount, 0);
        usersStaking[users_counter_staking] = account;
        users_counter_staking++;
    }

    // After vesting started p.1 p.2, interacting with staking
    function stake(address account, uint256 amount) public {
        require(!isStaking(account), "Already staking");
        require(isStarted(), "Need started contract, to interact");

        token.burn(account, amount);

        _stake(account, block.timestamp, amount);
    }

    // After vesting started p.1 p.2, interacting with staking
    function addAditionalAmountStaking(address account, uint256 amount) public {
        require(
            isGranted(account),
            "Admin can add additional reward only on granted accounts"
        );

        token.burn(account, amount);

        stakings[account].amount += amount + stakingTokensFromStaking(account);
        stakings[account].timeForStaking = block.timestamp;
    }

    // After vesting started p.1 p.2, interacting with staking
    function stakingTokensFromStaking(address account)
        public
        view
        returns (uint256)
    {
        require(isStaking(account), "Need to be staking");
        require(isStarted(), "Need started contract, to interact");

        uint256 staked_tokens = ((stakings[account].amount -
            stakings[account].withdrawn) *
            (block.timestamp - stakings[account].timeForStaking) *
            percent_per_year) /
            seconds_in_day /
            365 /
            100;

        return staked_tokens;
    }

    // After vesting started p.1 p.2, you can see how much available tokens
    function availableFromStaking(address account)
        public
        view
        returns (uint256)
    {
        require(isStaking(account), "Need to be staking");
        require(isStarted(), "Need started contract, to interact");

        uint256 availableTokens = stakings[account].amount +
            stakingTokensFromStaking(account) -
            vestings[account].withdrawn;

        return availableTokens;
    }

    // After vesting started p.1 p.2, you can get available tokens
    function getTokensFromStaking(address account, uint256 tokens) public {
        require(isStaking(account), "Need to be staking");
        require(isStarted(), "Need started contract, to interact");
        require(
            account == msg.sender,
            "Only owner of staking account can get tokens"
        );

        uint256 stakedTokens = stakingTokensFromStaking(account);

        require(tokens >= stakedTokens, "Need to take at least staked tokens");
        require(
            tokens <= availableFromStaking(account),
            "Too much, see how much is available in availableFromStaking function"
        );

        token.mint(account, tokens);

        stakings[account].amount =
            stakings[account].amount +
            stakedTokens -
            tokens;
        stakings[account].withdrawn =
            stakings[account].withdrawn +
            tokens -
            stakedTokens;

        stakings[account].timeForStaking = block.timestamp;
    }

    // VIEW INFORMATION FUNCTIONS
    // Necessary view functions p.3
    function isGranted(address account) public view returns (bool) {
        return (vestings[account].timeForStaking != 0);
    }

    function isStaking(address account) public view returns (bool) {
        return (stakings[account].timeForStaking != 0);
    }

    function getPeriodVesting(address account) public view returns (uint256) {
        return vestings[account].periodInDays;
    }

    function getLeftTimeVesting(address account) public view returns (uint256) {
        uint256 timeNeeded = start_vest_time +
            vestings[account].periodInDays *
            seconds_in_day;
        if (timeNeeded >= block.timestamp) {
            return timeNeeded - block.timestamp;
        } else {
            return 0;
        }
    }

    function getAmountVesting(address account) public view returns (uint256) {
        return vestings[account].amount;
    }

    function getWithdrawnVesting(address account)
        public
        view
        returns (uint256)
    {
        return vestings[account].withdrawn;
    }

    function getStackingTimeVesting(address account)
        public
        view
        returns (uint256)
    {
        return vestings[account].timeForStaking;
    }

    function getAmountStaking(address account) public view returns (uint256) {
        return stakings[account].amount;
    }

    function getWithdrawnStaking(address account)
        public
        view
        returns (uint256)
    {
        return stakings[account].withdrawn;
    }

    function getStackingTimeStaking(address account)
        public
        view
        returns (uint256)
    {
        return stakings[account].timeForStaking;
    }

    // Should eat a lot of gas?
    // All tokens on this contract
    function getFullAmount() public view returns (uint256) {
        uint256 fullAmount;
        for (uint256 i = 0; i < users_counter_vesting; i++) {
            address account = usersVesting[i];
            fullAmount +=
                vestings[account].amount +
                stakingTokensFromVesting(account) -
                vestings[account].withdrawn;
        }

        for (uint256 i = 0; i < users_counter_staking; i++) {
            address account = usersStaking[i];
            fullAmount +=
                stakings[account].amount +
                stakingTokensFromStaking(account) -
                stakings[account].withdrawn;
        }

        return fullAmount;
    }
}
