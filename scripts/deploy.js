const hre = require("hardhat");

async function main() {

    const deployer = await hre.ethers.getSigners();

    const Token = await hre.ethers.getContractFactory("Token");
    const Vesting = await hre.ethers.getContractFactory("Vesting");

    const token = await Token.deploy();
    const vesting = await Vesting.deploy(token.address);

    await token.deployed();
    await vesting.deployed();

    console.log("Token deployed to:", token.address);
    console.log("Vesting deployed to:", vesting.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });